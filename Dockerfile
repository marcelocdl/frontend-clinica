# Use the official Node.js image as base
FROM node:14.20.0-alpine AS builder

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Build the Angular application
RUN npm run build

# Use a smaller base image for the final application
FROM nginx:alpine

# Copy the built application to the Nginx web server's public directory
COPY --from=builder /app/dist/clinica /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Command to run the Nginx web server
CMD ["nginx", "-g", "daemon off;"]
